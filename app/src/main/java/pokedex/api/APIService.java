package pokedex.api;

import pokedex.model.PokemonReply;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {
    @GET("pokemon")
    Call<PokemonReply> getPokemonList(@Query("limit") int limit, @Query("offset") int offset);
}
