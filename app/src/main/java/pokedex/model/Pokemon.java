package pokedex.model;

public class Pokemon {

    private int id;
    private String name;
    private String url;

    public String getName() {
        return name;
    }
    public int getId() {
        String[] urlSegments = url.split("/");
        return Integer.parseInt(urlSegments[urlSegments.length-1]);
    }
}
