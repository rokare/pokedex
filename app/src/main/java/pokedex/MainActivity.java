package pokedex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import pokedex.R;

import pokedex.model.Pokemon;
import pokedex.model.PokemonReply;
import pokedex.api.APIService;
import pokedex.api.Adapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Query;

public class MainActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private static final String TAG = "POKEDEX";
    private RecyclerView recyclerView;
    private Adapter pokemonListAdapter;
    private int offset;
    private boolean readyToLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        pokemonListAdapter = new Adapter(this);
        recyclerView.setAdapter(pokemonListAdapter);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    if (readyToLoad) {
                        readyToLoad = false;
                        offset += 20;
                        getData(offset);
                    }
                }
            }
        });
        retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        readyToLoad = true;
        offset = 0;
        getData(offset);
    }

    private void getData(int offset) {
        APIService service = retrofit.create(APIService.class);
        Call<PokemonReply> pokemonResponseCall = service.getPokemonList(30, offset);
        Log.e(TAG, " getData()");
        pokemonResponseCall.enqueue(new Callback<PokemonReply>() {
            @Override
            public void onResponse(Call<PokemonReply> call, Response<PokemonReply> response) {
                Log.e(TAG, "onResponse()");
                readyToLoad = true;
                if(response.isSuccessful()) {
                    Log.e(TAG, "Successful");
                    PokemonReply pokemonResponse = response.body();
                    ArrayList<Pokemon> pokemonArrayList = pokemonResponse.getResults();
                    for(Pokemon p : pokemonArrayList) {
                        Log.i(TAG, p.getName());
                    }
                    pokemonListAdapter.addPokemonList(pokemonArrayList);

                } else {
                    Log.e(TAG, " error in onResponse method: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<PokemonReply> call, Throwable t) {
                Log.e(TAG, " error in onFailure method: " + t.getMessage());
                readyToLoad = true;
            }
        });
    }
}